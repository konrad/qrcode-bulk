module kolaente.dev/konrad/qrcode-bulk

go 1.14

require (
	github.com/magefile/mage v1.10.0
	github.com/schollz/progressbar/v3 v3.5.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	src.techknowlogick.com/xgo v1.1.1-0.20200909205326-e5dde9330cb0 // indirect
)
