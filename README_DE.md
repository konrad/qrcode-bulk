# Bulk QR Code Creator

Ein kleines Tool welches aus einem Ordner mit `.txt`-Dateien QR-Codes mit deren Inhalt erstellt.

# Anleitung

1. Besorg dir ein fertiges Binary für dein Betriebssystem von der [Release-Seite](https://kolaente.dev/konrad/qrcode-bulk/releases).
2. Entpacke die Zip-Datei in einen Ordner deiner Wahl.
3. Klicke doppelt auf die ausführbare Datei (Die einzige, die auf `.exe` endet bei Windows-Versionen) oder führe sie in einem Terminal aus.
4. Das Tool wird dich nach einem Ordner mit Textdateien fragen. Du kannst Enter drücken, um im aktuellen Ordner nach Textdateien zu suchen.
5. Während es die QR-Codes generiert wird es eine Fortschrittsanzeige anzeigen.
6. Wenn es fertig ist, werden alle QR-Codes im Ordner `qrcodes` im gleichen Ordner wie die Textdateien liegen
.
