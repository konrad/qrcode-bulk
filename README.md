# Bulk QR Code Creator

This is a small program which takes all `.txt` files in a folder and creates qr codes with their contents.

# Usage

1. Get a release for your os from the [releases page](https://kolaente.dev/konrad/qrcode-bulk/releases).
2. Unzip the files into a folder of your choice.
3. Double click on the executable file (The only one ending on `.exe` for windows releases) or execute it via a terminal.
4. The tool will ask you for a folder with text files. You can hit enter to use the current directory from where you ran the binary.
5. It will show a progress bar for all files.
6. Once done, all generated qr codes will be put into a folder called `qrcodes` in the same directory as the text files.
